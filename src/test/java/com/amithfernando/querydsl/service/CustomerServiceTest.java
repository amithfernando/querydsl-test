/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amithfernando.querydsl.service;

import com.amithfernando.querydsl.domain.Customers;
import com.amithfernando.querydsl.domain.QCustomers;
import com.querydsl.jpa.impl.JPAQuery;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author USER
 */
public class CustomerServiceTest {
    
    private static EntityManager em = null;
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        if (em == null) {
            em = (EntityManager) Persistence.createEntityManagerFactory("com.amithfernando_querydsl-test_jar_1.0PU").createEntityManager();
        }
    }
    
    @Test
    public void finaAll() {
        JPAQuery<?> query = new JPAQuery<Void>(em);
        QCustomers customers = QCustomers.customers;
        List<Customers> list = (List<Customers>) query.from(customers).fetch();
        Assert.assertEquals(122, list.size());
    }
    
    @Test
    public void findByCustomerName() {
        JPAQuery<?> query = new JPAQuery<Void>(em);
        QCustomers customers = QCustomers.customers;
        List<Customers> list = (List<Customers>) query.from(customers).where(customers.customerName.eq("Atelier graphique")).fetch();
        Assert.assertEquals(1, list.size());
    }
    
    @Test
    public void findLikeCustomerName() {
        JPAQuery<?> query = new JPAQuery<Void>(em);
        QCustomers customers = QCustomers.customers;
        List<Customers> list = (List<Customers>) query.from(customers).where(customers.customerName.like("A%")).fetch();
        Assert.assertEquals(16, list.size());
    }
    
     @Test
    public void findBySalesRepEmployeeId() {
        JPAQuery<?> query = new JPAQuery<Void>(em);
        QCustomers customers = QCustomers.customers;
        List<Customers> list = (List<Customers>) query.from(customers).where(customers.salesRepEmployeeNumber.employeeNumber.eq(1370)).fetch();
        Assert.assertEquals(7, list.size());
    }
    
}
